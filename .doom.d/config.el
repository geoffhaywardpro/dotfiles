;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Stephen Walsh"
      user-mail-address "stephen@connectwithawalsh.com")

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.
;;

(map! :g (kbd "<f8>") 'org-agenda
      :g (kbd "<f9> n") #'=notmuch
      :g (kbd "<f9> d") 'calendar
      :g (kbd "<f9> H") 'bh/hide-other
      :g (kbd "<f9> f") 'auto-fill-mode
      :g (kbd "<f9> c") 'counsel-org-clock-context
      :g (kbd "<f9> h") 'counsel-org-clock-history
      :g (kbd "<f9> g") 'counsel-org-clock-goto
      :g (kbd "<f9> o") 'bh/make-org-scratch
      :g (kbd "<f9> l") 'bh/clock-in-last-task)

(setq org-directory "/mnt/c/Users/StephenWalsh/Dropbox/org/"
      magit-repository-directories '("/mnt/c/Repos/")
      projectile-project-search-path '("/mnt/c/Repos/"))

(custom-set-faces!
  `(org-roam-link :foreground ,(doom-color 'green)))

(load! "+ui")
(load! "+org")
(load! "+magit")
(load! "+gnusalias")
(load! "+mail")
(load! "+ranger")
