;;; ~/.doom.d/+magit.el -*- lexical-binding: t; -*-

(setq projectile-indexing-method 'native
      projectile-enable-caching t)
