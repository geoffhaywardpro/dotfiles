;;; ~/.doom.d/+org.el -*- lexical-binding: t; -*-

(after! org
  (run-at-time "00:19" 3600 'org-save-all-org-buffers)
  (run-at-time "00:39" 3600 'org-save-all-org-buffers)
  (run-at-time "00:59" 3600 'org-save-all-org-buffers)

  (setq org-agenda-files (list org-directory)
        ;; org-roam-directory (concat org-directory "zettelkasten")
        org-ellipsis " ⤵"
        org-modules (quote (org-habit))
        org-todo-keywords (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                                  (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING")))

        org-todo-state-tags-triggers (quote (("CANCELLED" ("CANCELLED" . t))
                                             ("WAITING" ("WAITING" . t))
                                             ("HOLD" ("WAITING") ("HOLD" . t))
                                             (done ("WAITING") ("HOLD"))
                                             ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                                             ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                                             ("DONE" ("WAITING") ("CANCELLED") ("HOLD"))))
        org-capture-templates `(("t" "todo" entry (file ,(concat org-directory "refile.org"))
                                 "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
                                ("r" "respond" entry (file ,(concat org-directory "refile.org"))
                                 "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
                                ("n" "note" entry (file ,(concat org-directory "refile.org"))
                                 "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
                                ("j" "Journal" entry (file+olp+datetree ,(concat org-directory "journal.org"))
                                 "* %?\n%U\n" :clock-in t :clock-resume t)
                                ("m" "Meeting" entry (file ,(concat org-directory "refile.org"))
                                 "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
                                ("h" "Habit" entry (file ,(concat org-directory "refile.org"))
                                 "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))

        ;; Custom agenda command definitions
        org-agenda-custom-commands '(("8" "SCALE YOURSELF"
                                      ((agenda "" ((org-agenda-ndays 1)))
                                      (tags "PRIORITY=\"A\""
                                             ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                                              (org-agenda-overriding-header "TASKS FOR A PRODUCTIVE FEELING AT THE END OF THE DAY")))
                                       (tags "REFILE"
                                             ((org-agenda-overriding-header "TASKS TO REFILE")
                                              (org-tags-match-list-sublevels nil)))
                                       (alltodo ""
                                                ((org-agenda-skip-function
                                                  '(or (air-org-skip-subtree-if-habit)
                                                       (air-org-skip-subtree-if-priority ?A)
                                                       (org-agenda-skip-if nil '(scheduled deadline))))
                                                 (org-agenda-sorting-strategy '(todo-state-up))
                                                 (org-agenda-overriding-header "ALL NORMAL PRIORITY TASKS"))))))

        org-agenda-start-day nil
        org-agenda-compact-blocks t
        org-agenda-clockreport-parameter-plist (quote (:link t :maxlevel 5 :fileskip0 t :compact t :narrow 80))
        org-agenda-tags-todo-honor-ignore-options t
        org-agenda-span 'day

        org-archive-mark-done nil

        org-treat-S-cursor-todo-selection-as-state-change nil
        org-use-fast-todo-selection t

        org-refile-allow-creating-parent-nodes 'confirm
        org-refile-targets (quote ((nil :maxlevel . 9)
                                   (org-agenda-files :maxlevel . 9)))
        org-refile-target-verify-function 'bh/verify-refile-target

        org-clock-history-length 21
        org-clock-in-resume t
        org-clock-into-drawer t
        org-clock-out-remove-zero-time-clocks t
        org-clock-out-when-done t
        org-clock-persist t
        org-clock-persist-query-resume nil
        org-clock-auto-clock-resolution 'when-no-clock-is-running
        org-clock-report-include-clocking-task t
        org-drawers (quote ("PROPERTIES" "LOGBOOK"))
        org-log-done 'time
        org-log-into-drawer t
        org-log-state-notes-insert-after-drawers nil
        org-tag-alist (quote ((:startgroup
                               ("@errand" . ?e)
                               ("@office" . ?o)
                               ("@home" . ?H)
                               (:endgroup)
                               ("WAITING" . ?w)
                               ("HOLD" . ?h)
                               ("ADMIN" . ?A)
                               ("DEEPWORK" . ?W)
                               ("FUN" . ?F)
                               ("crypt" . ?E)
                               ("NOTE" . ?n)
                               ("CANCELLED" . ?c)
                               ("FLAGGED" . ??))))
        org-fast-tag-selection-single-key 'expert
        org-archive-location "%s_archive::* Archived Tasks"
        org-journal-dir (concat org-directory "journal")
        org-journal-file-format "%Y-%m-%d.org"
        org-journal-date-prefix "#+TITLE: "
        org-journal-date-format "%A, %B %d %Y"
        org-journal-time-prefix "* "
        )

  (defun bh/verify-refile-target ()
    "Exclude todo keywords with a done state from refile targets"
    (not (member (nth 2 (org-heading-components)) org-done-keywords)))

  (defun air-org-skip-subtree-if-priority (priority)
    "Skip an agenda subtree if it has a priority of PRIORITY.

PRIORITY may be one of the characters ?A, ?B, or ?C."
    (let ((subtree-end (save-excursion (org-end-of-subtree t)))
          (pri-value (* 1000 (- org-lowest-priority priority)))
          (pri-current (org-get-priority (thing-at-point 'line t))))
      (if (= pri-value pri-current)
          subtree-end
        nil)))

  (defun air-org-skip-subtree-if-habit ()
    "Skip an agenda entry if it has a STYLE property equal to \"habit\"."
    (let ((subtree-end (save-excursion (org-end-of-subtree t))))
      (if (string= (org-entry-get nil "STYLE") "habit")
          subtree-end
        nil)))
  )

(use-package! company-org-roam
  ;; :straight (:host github :repo "jethrokuan/company-org-roam")
  :config
  (push 'company-org-roam company-backends))


(map! :after org-agenda
      :map org-agenda-mode-map
      :localleader
      "T" #'org-agenda-log-mode)
