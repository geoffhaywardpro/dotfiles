;;; ~/.doom.d/+mail.el -*- lexical-binding: t; -*-

(use-package! notmuch
  :commands (notmuch
             notmuch-tree
             notmuch-tree-mode
             notmuch-search
             notmuch-search-mode
             notmuch-hello
             notmuch-hello-mode
             notmuch-show
             notmuch-show-tode
             notmuch-message-mode)
  :config
  (setq notmuch-saved-searches
        '((:name "personal | inbox" :query "tag:new AND (to:stephen@connectwithawalsh.com OR to:stephen.walsh@icloud.com)" :key "I")
          (:name "amf | inbox" :query "tag:new AND NOT (tag:health-monitor OR tag:deployment OR tag:tfs OR tag:spam) AND (to:swalsh@flex125.com OR to:swalsh@myameriflex.com OR from:@firstdata.com) :key "i"")
          (:name "flagged" :query "tag:flagged" :key "f")
          (:name "amf | prs" :query "tag:tfs AND tag:new AND  subject:'PR\ \-.*'" :key "p")
          (:name "amf | health-monitor" :query "tag:health-monitor AND tag:new" :key "h")
          (:name "amf | tfs" :query "tag:tfs AND tag:new" :key "t")
          (:name "amf | deployments" :query "tag:deployment AND tag:new" :key "d")
          (:name "personal | sent" :query "tag:sent AND (from:stephen@connectwithawalsh.com OR from:stephen.walsh@icloud.com)")
          (:name "personal | archive" :query "tag:archived AND (to:stephen@connectwithawalsh.com OR to stephen.walsh@icould.com)")
          (:name "amf | sent" :query "tag:sent AND (from:swalsh@flex125.com OR from:swalsh@myameriflex.com)" :sort-order newest-first)
          (:name "all new" :query "tag:unread OR tag:new OR tag:inbox" :key "n")
          (:name "all mail" :query "*" :key "a" :sort-order newest-first))
        notmuch-message-deleted-tags '("+trash" "-inbox" "-unread" "-new")
        notmuch-archive-tags '("-inbox" "-unread" "-new")
        notmuch-fcc-dirs nil

        mail-envelope-from 'header
        mail-specify-envelope-from t
        message-directory "~/.mail"
        message-kill-buffer-on-exit t
        message-sendmail-envelope-from 'header
        sendmail-program "/usr/bin/msmtp"
        send-mail-function 'sendmail-send-it)
  (map! (:after notmuch
          (:map notmuch-show-mode-map
            :nmv "o"     #'ace-link-notmuch-show
            :nmv "i"     #'+mail/open-message-with-mail-app-notmuch-show
            :nmv "I"     #'notmuch-show-view-all-mime-parts
            :nmv "q"     #'notmuch-bury-or-kill-this-buffer
            (:when (featurep! :completion ivy)
              :nmv "s"     #'counsel-notmuch)
            (:when (featurep! :completion helm)
              :nmv "s"     #'helm-notmuch)
            :nmv "t"     #'notmuch-tree-from-show-current-query
            :nmv "N"   #'notmuch-mua-new-mail
            :nmv "n"     #'notmuch-show-next-thread-show
            :nmv "r"     #'notmuch-show-reply
            :nmv "<tab>" #'notmuch-show-toggle-visibility-headers
            :nmv "R"     #'notmuch-show-reply-sender
            :nmv "p"   #'notmuch-show-previous-thread-show)
          (:map notmuch-hello-mode-map
            :nmv "o"   #'ace-link-notmuch-hello
            :nmv "t"   #'notmuch-tree
            :nmv "k"   #'widget-backward
            :nmv "n"   #'notmuch-mua-new-mail
            :nmv "N" #'notmuch-mua-new-mail
            :nmv "j"   #'widget-forward
            (:when (featurep! :completion ivy)
              :nmv "s"     #'counsel-notmuch)
            (:when (featurep! :completion helm)
              :nmv "s"     #'helm-notmuch)
            :nmv "q"   #'+mail/quit
            :nmv "r"   #'notmuch-hello-update)
          (:map notmuch-search-mode-map
            :nmv "j"   #'notmuch-search-next-thread
            :nmv "k"   #'notmuch-search-previous-thread
            :nmv "t"   #'notmuch-tree-from-search-thread
            ;; :nmv "RET"   #'notmuch-tree-from-search-thread
            :nmv "RET" #'notmuch-search-show-thread
            :nmv "N" #'notmuch-mua-new-mail
            :nmv "T"   #'notmuch-tree-from-search-current-query
            :nmv ";"   #'notmuch-search-tag
            :nmv ","   #'notmuch-jump-search
            :nmv "d"   #'+mail/notmuch-search-delete
            :nmv "a"   #'notmuch-search-archive-thread
            ;; :nmv "q"   #'notmuch
            :nmv "q"   #'+mail/quit
            :nmv "R"   #'notmuch-search-reply-to-thread-sender
            :nmv "r"   #'notmuch-search-reply-to-thread
            :nmv "go"  #'+notmuch-exec-offlineimap
            (:when (featurep! :completion ivy)
              :nmv "s"     #'counsel-notmuch)
            (:when (featurep! :completion helm)
              :nmv "s"     #'helm-notmuch)
            :nmv "x"   #'+mail/notmuch-search-spam)
          (:map notmuch-tree-mode-map
            :nmv "j"   #'notmuch-tree-next-message
            :nmv "k"   #'notmuch-tree-prev-message
            :nmv "S"   #'notmuch-search-from-tree-current-query
            (:when (featurep! :completion ivy)
              :nmv "s"     #'counsel-notmuch)
            (:when (featurep! :completion helm)
              :nmv "s"     #'helm-notmuch)
            :nmv "t"   #'notmuch-tree
            :nmv ";"   #'notmuch-tree-tag
            :nmv "RET" #'notmuch-tree-show-message
            :nmv "q"   #'notmuch-tree-quit
            :nmv "s-n" #'notmuch-mua-new-mail
            :nmv "r"   #'notmuch-search-reply-to-thread-sender
            :nmv "a"   #'notmuch-tree-archive-message-then-next
            :nmv "A"   #'notmuch-tree-archive-thread
            :nmv "i"   #'+mail/open-message-with-mail-app-notmuch-tree
            :nmv "d"   #'+mail/notmuch-tree-delete
            :nmv "x"   #'+mail/notmuch-tree-spam)
          (:map notmuch-message-mode-map
            :localleader
            :desc "Send and Exit"       doom-localleader-key #'notmuch-mua-send-and-exit
            :desc "Kill Message Buffer" "k" #'notmuch-mua-kill-buffer
            :desc "Save as Draft"       "s" #'message-dont-send
            :desc "Attach file"         "f" #'mml-attach-file))))
  ;;;; counsel-notmuch
  (when (featurep! :completion ivy)
    (use-package! counsel-notmuch
      :commands counsel-notmuch
      :after notmuch))

;;;###autoload
(defun =mail ()
  "Activate (or switch to) `notmuch' in its workspace."
  (interactive)
  (if-let* ((buf (cl-find-if (lambda (it) (string-match-p "^\\*notmuch" (buffer-name (window-buffer it))))
                             (doom-visible-windows))))
      (select-window (get-buffer-window buf))
    (notmuch-search "tag:inbox")))
;; (call-interactively 'notmuch-hello-sidebar)

;;;###autoload
(defun +mail/quit ()
  (interactive)
  ;; (+popup/close (get-buffer-window "*notmuch-hello*"))
  (doom-kill-matching-buffers "^\\*notmuch"))
