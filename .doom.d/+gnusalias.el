;;; ~/.doom.d/+gnusalias.el -*- lexical-binding: t; -*-

(after! gnus-alias
  (autoload 'gnus-alias-determine-identity "gnus-alias" "" t)
  (setq gnus-alias-identity-alist
        '(("home"
           null
           "Stephen Walsh <stephen@connectwithawalsh.com>"
           nil
           (("Fcc" . "connect/sent"))
           nil
           nil)
          ("work"
           nil
           "Stephen Walsh <swalsh@myameriflex.com"
           "Ameriflex"
           nil
           nil
           nil))
        gnus-alias-default-identity "home"
        gnus-alias-identity-rules '(("work" ("any" "swalsh@\\(flex125\\.com\\|myameriflex.com\\)" both) "work"))
        gnus-alias-override-user-mail-address t)
  (add-hook! 'message-setup-hook 'gnus-alias-determine-identity)
)
